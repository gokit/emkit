Emkit
--------

[![Go Report Card](https://goreportcard.com/badge/github.com/gokit/emkit)](https://goreportcard.com/report/github.com/gokit/emkit)
[![Travis Build Status](https://travis-ci.org/gokit/emkit.svg?branch=master)](https://travis-ci.org/gokit/emkit#)

Emkit is a small library geared towards providing a single central backbone for assigning configured objects and values
into reusable and passable buckets. This buckets will create a new value from arguments provided based on a predefined
function, this makes them useful as a configuration companion, who do not replace your `Config` struct but rather
reduce your re-writting of same configuration functions that create values for every project. Their basic purpose is to
keep things [DRY](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself) and more so, can be passed around into
different functions or packages to retrieve stored values whilst able to wrap them with functions that ensure type safety.


## Install

```go
go get -v github.com/gokit/emkit
```

## Configs

Included as subpackages, emkit contains the following configuration options built in:

#### [NATS](./nats)

This provides a configuration setup for creating a single NATS client `net.Conn`
to a specified address to be passed around.

#### [Databox](./databox)
This provides a container for data files read into memory to quick access.

#### [Enco](./enco)
This provides a container for pem encoded encryption keys (public and private) read into
memory for quick access across your code.

#### [Bajieve](./bajieve)

This provides a container to hold badger database and bleve indexes for access across your
code.

## Usage


```go
import (
    "github.com/gokit/emkit"
    "github.com/some/config/nats"
)

var (
    natsId = "waller"
    clusterId = "gosh"
)

func main(){
    // once a bucket is created with a unique name, it is
    // remembered and can be used again to get the same bucket
    // again.
    bucket := emkit.Bucket("servo-service")
    defer bucket.Discard();

    if err := nats.InitNATStreamer(bucket, clusterId, natsId); err != nil {
        log.Fatal(err)
    }

    conn := nats.GetNATStreamer(bucket)
    // do something with conn.
}
```

```go
import (
    "github.com/gokit/emkit"
     stan "github.com/nats-io/go-nats-streaming"
)

// configuration types.
const (
    NATStreamingConfig = emkit.ConfigID("NATS:Streaming")
)

// CreateNATStreamer creates a new nats streaming connection to provided cluster and server ids, returning
// an error if failed.
func CreateNATStreamer(mybucket emkit.ConfigBucket, clusterId string, serverClientId string) (*stan.Conn, error) (
	return stan.Connect(clusterId, serverClientId)
}

// InitNATStreamer initiates nats streamer into provided bucket using CreateNATStreamer
// as configuration.
func InitNATStreamer(bucket *emkit.ConfigBucket, args ...interface{}) error {
    return bucket.Attach(NATStreamingConfig, args...)
}

// GetNATStreamer expects supplied bucket to have the handle for the needed nats server setup, else it panics
// that it was not found. Since this is to be used as a means of configuration, panicing here make sense as we
// definitely want to know what happened.
func GetNATStreamer(bucket *emkit.ConfigBucket) *stan.Conn {
    conn, err := bucket.Get(NATStreamingConfig)
    if err != nil {
        panic(err)
    }
    return conn
}

// CloseStream will close existing nats streaming connection in bucket.
func CloseStream(bucket *emkit.ConfigBucket) {
	if conn, err := bucket.Get(NATStreamingConfig); err == nil && conn != nil {
		conn.(natstream.Conn).Close()
	}
}

func init(){
    // Register the configuration function with disposer, to ensure nats connection
    // gets closed.
    emkit.Register(NATStreamingConfig, CreateNATStreamer, func(bu *emkit.ConfigBucket){
        CloseStream(bu);
    });
}
```