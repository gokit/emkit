package emkit

import (
	"reflect"

	"sync"

	"github.com/influx6/faux/reflection"
)

// errors ...
const (
	ErrConfigIDInUse        = Error("ConfigID already being used")
	ErrNilNotAllowed        = Error("ConfigBucket won't store a null/nil value")
	ErrDiscardedBucket      = Error("ConfigBucket already discarded, now un-usable")
	ErrNoSuchDraft          = Error("Configuration Function not registered")
	ErrNotAttached          = Error("Configuration value not attached to bucket")
	ErrMustReturn           = Error("Configuration Function with one return type must return non-error type")
	ErrMustTakeConfigBucket = Error("Configuration Function must take ConfigBucket as first argument")
	ErrMustReturnWithError  = Error("Configuration Function with two returns must return an error as last type")
)

// Error defines a string type to represent a giving error type
type Error string

// Error returns itself as a string.
func (e Error) Error() string {
	return string(e)
}

var (
	defaultDesc = "NO DESCRIPTION"
	errorType   = reflect.TypeOf((*error)(nil)).Elem()
	bucketType  = reflect.TypeOf((*ConfigBucket)(nil))

	fnArgumentRules = []reflection.TypeValidation{
		func(types []reflect.Type) error {
			argLen := len(types)
			if argLen > 0 {
				if reflection.IsSettableType(bucketType, types[0]) {
					return nil
				}
			}
			return ErrMustTakeConfigBucket
		},
	}

	fnReturnRules = []reflection.TypeValidation{
		func(types []reflect.Type) error {
			retLen := len(types)
			switch retLen {
			case 1:
				// if it's a single value then it can not be
				// an error.
				if !reflection.IsSettableType(errorType, types[0]) {
					return nil
				}
			case 2:
				// if we have a function returning two values, then
				// the last one must be an error type.
				if reflection.IsSettableType(errorType, types[1]) {
					return nil
				}
				return ErrMustReturnWithError
			}
			return ErrMustReturn
		},
	}
)

//***************************************************
// ConfigBucket
//***************************************************

var (
	buckets = map[string]*ConfigBucket{}
)

// Stash provides a privilege function to allow adding a custom
// value into a giving bucket without going through the configuration
// function steps. It is meant to allow users to stash items they wish
// to have exposed through a bucket with a unique id, note if an existing
// item exists in the bucket with the id, an error is returned.
// If the m... is provided then only the first is used has disposer.
func Stash(bucket *ConfigBucket, id ConfigID, v interface{}, disposers ...Disposer) error {
	if v == nil {
		return ErrNilNotAllowed
	}

	var disposer Disposer
	if len(disposers) != 0 {
		disposer = disposers[0]
	}

	bucket.cl.Lock()
	defer bucket.cl.Unlock()

	if _, ok := bucket.config[id]; !ok {
		bucket.config[id] = v
		if disposer != nil {
			bucket.disposers[id] = disposer
		}
		return nil
	}
	return ErrConfigIDInUse
}

// Bucket returns an existing bucket with giving
func Bucket(bucketId string) *ConfigBucket {
	bucket, ok := buckets[bucketId]
	if !ok {
		nc := newConfigBucket()
		buckets[bucketId] = nc
		return nc
	}

	// To ensure race safety, we must lazily remove
	// a discard bucket when it's called. Even if its
	// never it's internals would have already being
	// discard, free for GC, so no cost here.
	if ok && bucket.Discarded() {
		delete(buckets, bucketId)
		nc := newConfigBucket()
		buckets[bucketId] = nc
		return nc
	}

	return bucket
}

// Disposer defines a function type that takes in a pointer to a ConfigBucket.
type Disposer func(bucket *ConfigBucket)

// ConfigBucket implements the ConfigBucket interface.
// ConfigBuckets are safe for concurrent use.
type ConfigBucket struct {
	cl        sync.Mutex
	config    map[ConfigID]interface{}
	disposers map[ConfigID]Disposer
}

func newConfigBucket() *ConfigBucket {
	return &ConfigBucket{
		config:    map[ConfigID]interface{}{},
		disposers: map[ConfigID]Disposer{},
	}
}

// Discard is a forceful means of destroy both the buckets and
// it's internal values, you must take care to only call this
// in your main.go file using defer, has all values will be lost
// and if there are items stored which may cause a leak, then prior
// before a call to this method, that item must have been dealt with
// accordingly using the appropriate means dictated by it's methods or
// through the Dispose Handler provided when registering values to dictate
// to a bucket to handle when being discarded.
// Discard will remove this bucket from the global store and wipe out
// all contents.
// NOTE: Always ensure to call this to properly garbage collect buckets
// but do so in your main.go or be very sure to know what you are doing by
// calling it somewhere else.
func (c *ConfigBucket) Discard() {
	c.cl.Lock()
	for _, disposer := range c.disposers {
		c.cl.Unlock()
		disposer(c)
		c.cl.Lock()
	}
	c.config = nil
	c.cl.Unlock()
}

// Attach attempts to resolve the configuration
// registered with giving ConfigID, attaching
// the returned value to the bucket if no error
// occurred, else returning said error for user's
// action.
func (c *ConfigBucket) Attach(id ConfigID, args ...interface{}) error {
	c.cl.Lock()
	if c.config == nil {
		c.cl.Unlock()
		return ErrDiscardedBucket
	}
	c.cl.Unlock()

	source, err := drafts.Draft(id)
	if err != nil {
		return err
	}

	// create new list of arguments to be passed to function
	// prefixing the bucket as first.
	newArg := make([]interface{}, len(args)+1)
	newArg[0] = c
	copy(newArg[1:], args)

	res, err := reflection.CallFunc(source.Fn, newArg...)
	if err != nil {
		return err
	}

	c.cl.Lock()
	defer c.cl.Unlock()

	switch len(res) {
	case 1:
		c.config[id] = res[0]
		if source.Disposer != nil {
			c.disposers[id] = source.Disposer
		}
	case 2:
		opErr := res[1]
		if opErr != nil {
			return opErr.(error)
		}

		c.config[id] = res[0]
		if source.Disposer != nil {
			c.disposers[id] = source.Disposer
		}
	}
	return nil
}

// MustGet returns the associated value of the id else panics if not found.
func (c *ConfigBucket) MustGet(id ConfigID) interface{} {
	val, err := c.Get(id)
	if err != nil {
		panic(err)
	}
	return val
}

// Has returns true/false if giving id exists in bucket.
func (c *ConfigBucket) Has(id ConfigID) bool {
	c.cl.Lock()
	defer c.cl.Unlock()

	if c.config == nil {
		return false
	}

	_, ok := c.config[id]
	return ok
}

// Get returns the attached value for giving
// ConfigID after it has being resolved.
// Rather than return nil for not seen, it
// returns an error if not registered in bucket.
func (c *ConfigBucket) Get(id ConfigID) (interface{}, error) {
	c.cl.Lock()
	defer c.cl.Unlock()

	if c.config == nil {
		return nil, ErrDiscardedBucket
	}

	item, ok := c.config[id]
	if !ok {
		return nil, ErrNotAttached
	}
	return item, nil
}

// Discarded returns true/false if bucket has being discarded.
func (c *ConfigBucket) Discarded() bool {
	var discarded bool
	c.cl.Lock()
	discarded = c.config == nil
	c.cl.Unlock()
	return discarded
}

//***************************************************
// MasterDrafts and Drafts
//***************************************************

var drafts = masterDraft{
	drafts: map[ConfigID]draft{},
}

// ConfigID type defines a string type key.
type ConfigID string

// ListDrafts returns a map of all registered configuration drafts
// with associated description.
// It is not safe for concurrent use.
func ListDrafts() map[string]string {
	return drafts.List()
}

// Register registers new function with optional description which
// will be used by ConfigBuckets to generated configuration values
// based on associated ConfigID.
// It is not safe for concurrent use.
func Register(id ConfigID, fn interface{}, dn Disposer, desc ...string) error {
	return drafts.Register(id, fn, dn, desc...)
}

type draft struct {
	Desc     string
	Fn       interface{}
	Disposer Disposer
}

// masterDraft defines a configuration store which
// contains stored configuration drafts which contain
// associated descriptions and functions to create
// based on provided arguments.
type masterDraft struct {
	drafts map[ConfigID]draft
}

// List returns a map of all draft's IDs and descriptions.
// It's intended to allow user see a list of registered
// drafts.
func (md *masterDraft) List() map[string]string {
	set := map[string]string{}
	for id, draft := range md.drafts {
		set[string(id)] = draft.Desc
	}
	return set
}

// Draft returns associated draft registered to master drafts.
func (md *masterDraft) Draft(m ConfigID) (draft, error) {
	draftd, ok := md.drafts[m]
	if !ok {
		return draftd, ErrNoSuchDraft
	}
	return draftd, nil
}

// Register adds giving function into the drafts registering.
func (md *masterDraft) Register(m ConfigID, fn interface{}, disposer Disposer, desc ...string) error {
	if err := reflection.ValidateFunc(fn, fnArgumentRules, fnReturnRules); err != nil {
		return err
	}

	md.drafts[m] = draft{Desc: getDesc(desc), Fn: fn, Disposer: disposer}
	return nil
}

func (md *masterDraft) remove(m ConfigID) {
	delete(md.drafts, m)
}

func getDesc(m []string) string {
	if len(m) == 0 {
		return defaultDesc
	}
	return m[0]
}
