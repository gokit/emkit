package emkit_test

import (
	"testing"

	"github.com/gokit/emkit"
	"github.com/stretchr/testify/assert"
)

func TestBucketSameness(t *testing.T) {
	bucket := emkit.Bucket("my-app")

	bucket2 := emkit.Bucket("my-app")
	assert.Equal(t, bucket2, bucket)

	bucket3 := emkit.Bucket("my-app")
	assert.Equal(t, bucket3, bucket)
	assert.Equal(t, bucket3, bucket2)

	bucket.Discard()

	bucket4 := emkit.Bucket("my-apps")
	defer bucket4.Discard()
	assert.NotEqual(t, bucket4, bucket)
	assert.NotEqual(t, bucket4, bucket2)
	assert.NotEqual(t, bucket4, bucket3)
}

func TestBucketStashing(t *testing.T) {
	bucket := emkit.Bucket("my-app")
	defer bucket.Discard()

	id := emkit.ConfigID("Buss")
	res, err := bucket.Get(id)
	assert.Error(t, err)
	assert.Nil(t, res)

	emkit.Stash(bucket, id, "Shoe")
	res, err = bucket.Get(id)
	assert.NoError(t, err)
	assert.NotNil(t, res)
	assert.Equal(t, "Shoe", res)
}

func TestBucketDiscarding(t *testing.T) {
	bucket := emkit.Bucket("my-app")

	id := emkit.ConfigID("Buss")
	emkit.Stash(bucket, id, "Shoe")

	res, err := bucket.Get(id)
	assert.NoError(t, err)
	assert.NotNil(t, res)
	assert.Equal(t, "Shoe", res)

	bucket.Discard()
	assert.Error(t, bucket.Attach(id))
	assert.Equal(t, emkit.ErrDiscardedBucket, bucket.Attach(id))

	_, err2 := bucket.Get(id)
	assert.Error(t, err2)
	assert.Equal(t, emkit.ErrDiscardedBucket, err2)

	bucket2 := emkit.Bucket("my-app")
	assert.NotEqual(t, bucket2, bucket)
	bucket2.Discard()
}

func TestConfigBucket(t *testing.T) {
	id := emkit.ConfigID("Buss")
	emkit.Register(id, func(bucket *emkit.ConfigBucket, item string) string {
		return "Wears-" + item
	}, nil)

	bucket := emkit.Bucket("my-app")
	defer bucket.Discard()

	assert.NoError(t, bucket.Attach(id, "shoe"))

	val, err := bucket.Get(id)
	assert.NoError(t, err)
	assert.Equal(t, "Wears-shoe", val)
}
