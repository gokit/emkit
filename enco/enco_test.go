package enco_test

import (
	"testing"

	"github.com/gokit/emkit"
	"github.com/gokit/emkit/enco"
	"github.com/stretchr/testify/assert"
)

func TestGetEncryptionBox(t *testing.T) {
	bucket := emkit.Bucket("myapp")
	defer bucket.Discarded()

	assert.NoError(t, enco.LoadEncryptions(bucket, enco.Add{
		Name:       "keys.server-tls",
		Dir:        "./data",
		PrvKeyName: "prv.pem",
		PubKeyName: "pub.pem",
	}))

	box, err := enco.GetEncryptionBox(bucket)
	assert.NoError(t, err)
	assert.NotNil(t, box)

	encd, err := box.Encryption("keys.server-tls")
	assert.NoError(t, err)
	assert.NotNil(t, encd)
	assert.NotNil(t, encd.PublicKey)
	assert.NotNil(t, encd.PrivateKey)
	assert.Equal(t, encd.PublicKey.Type, "PUBLIC KEY")
	assert.Equal(t, encd.PrivateKey.Type, "RSA PRIVATE KEY")
}
