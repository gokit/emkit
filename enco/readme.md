Enco
---------
Load your pem encoded public and private keys into memory with configuration buckets with ease.


## Usage

```go
import (
	"log"
	"github.com/gokit/emkit"
	"github.com/gokit/emkit/enco"
)

bucket := emkit.Bucket("myapp")
defer bucket.Discarded()

err := enco.LoadEncryptions(bucket, enco.Add{
		Name:       "keys.server-tls",
		Dir:        "./data",
		PrvKeyName: "prv.pem",
		PubKeyName: "pub.pem",
})
if err != nil {
    log.Fatal(err)
}

box, err := databox.GetDataBox(bucket)
if err != nil {
    log.Fatal(err)
}


priv, err := box.Box("keys.server-tls")
if err != nil {
    log.Fatal(err)
}

priv.PublicKey // get the public key pem.Block
priv.PrivateKey // get the private key pem.Block

```
