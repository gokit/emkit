package enco

import (
	"io/ioutil"
	"path/filepath"

	"encoding/pem"

	"github.com/gokit/emkit"
)

const (
	encryptionConfigKey = emkit.ConfigID("encryption-config")
	rsaCertKeyName      = "RSA PRIVATE KEY"
	publicKeyName       = "PUBLIC KEY"
	ecCertKeyName       = "EC PRIVATE KEY"
)

//******************************************************
// errors
//******************************************************

// errors
var (
	ErrNoAddProvided              = Error("No Add instructions provided")
	ErrNotFound                   = Error("Not Found")
	ErrNoNameProvided             = Error("Add.Name not provided")
	ErrNoPrvOrPubProvided         = Error("Add.PrvKeyName or Add.PubKeyName not provided")
	ErrNotValidPemBlock           = Error("byte slice is not valid pem block")
	ErrNotValidPemPublicKeyBlock  = Error("byte slice is not valid pem public key block")
	ErrNotValidPemPrivateKeyBlock = Error("byte slice is not valid pem private key block")
)

// Error implements error interface.
type Error string

// Error returns a string version of error.
func (e Error) Error() string {
	return string(e)
}

//******************************************************
// key types
//******************************************************

// private key type constants.
const (
	rSAKeyType = iota
	eCDSAKeyType
	publicKeyType
)

func keyType(m string) int {
	switch m {
	case rsaCertKeyName:
		return rSAKeyType
	case publicKeyName:
		return publicKeyType
	case ecCertKeyName:
		return eCDSAKeyType
	}
	return -1
}

//******************************************************
// Encryption and EncryptionBox
//******************************************************

// Encryption defines a struct which holds byte values
// of public and private key.
type Encryption struct {
	PublicKey  *pem.Block
	PrivateKey *pem.Block
}

// EncryptionBox defines an interface which exposes a mnethod to
// retrieve a giving encryption key(public and private) loaded during
// setup to allow access.
type EncryptionBox interface {
	Encryption(name string) (Encryption, error)
}

// Add embodies data used to load giving encryption keys
// located within specified directory names. It provides
// associated fields to hold values of directory
// public key and private key name, which if either one is
// non-present then that key would not be loaded.
// But note if neither private or public key names is provided
// then Add would be skipped, more so if name is not present
// it will be skipped as well.
type Add struct {
	Name       string
	Dir        string
	PubKeyName string
	PrvKeyName string
}

func (c Add) valid() error {
	if c.Name == "" {
		return ErrNoNameProvided
	}
	if c.PrvKeyName == "" && c.PubKeyName == "" {
		return ErrNoPrvOrPubProvided
	}
	return nil
}

// GetEncryptionBox attempts to retrieve loaded EncryptionBox from bucket
// if available else returning an error.
func GetEncryptionBox(bucket *emkit.ConfigBucket) (EncryptionBox, error) {
	item, err := bucket.Get(encryptionConfigKey)
	if err != nil {
		return nil, err
	}
	return item.(EncryptionBox), nil
}

// LoadEncryptions will initializes giving bucket with a EncryptionBox containing
// all loaded encryption data from provided Add slice. It returns an error if
// any failed and if successful attaches EncryptionBox to bucket.
func LoadEncryptions(bucket *emkit.ConfigBucket, sets ...Add) error {
	if bucket.Discarded() {
		return emkit.ErrDiscardedBucket
	}
	if bucket.Has(encryptionConfigKey) {
		return nil
	}
	return bucket.Attach(encryptionConfigKey, sets)
}

func loadEncryptionKeys(bucket *emkit.ConfigBucket, sets ...Add) (EncryptionBox, error) {
	if len(sets) == 0 {
		return nil, ErrNoAddProvided
	}

	box := make(encryptionBox)

	for _, elem := range sets {
		if err := elem.valid(); err != nil {
			return nil, err
		}

		var enc Encryption
		if elem.PubKeyName != "" {
			prvData, err := ioutil.ReadFile(pathJoin(elem.Dir, elem.PubKeyName))
			if err != nil {
				return nil, err
			}

			pemBlock, _ := pem.Decode(prvData)
			if pemBlock == nil {
				return nil, ErrNotValidPemBlock
			}

			pemType := keyType(pemBlock.Type)
			if pemType == -1 || pemType != publicKeyType {
				return nil, ErrNotValidPemPublicKeyBlock
			}

			enc.PublicKey = pemBlock
		}

		if elem.PrvKeyName != "" {
			prvData, err := ioutil.ReadFile(pathJoin(elem.Dir, elem.PrvKeyName))
			if err != nil {
				return nil, err
			}

			pemBlock, _ := pem.Decode(prvData)
			if pemBlock == nil {
				return nil, ErrNotValidPemBlock
			}

			pemType := keyType(pemBlock.Type)
			if pemType == -1 || pemType == publicKeyType {
				return nil, ErrNotValidPemPrivateKeyBlock
			}

			enc.PrivateKey = pemBlock
		}

		box[elem.Name] = enc
	}

	return box, nil
}

type encryptionBox map[string]Encryption

func (b encryptionBox) Encryption(name string) (Encryption, error) {
	if db, ok := b[name]; ok {
		return db, nil
	}
	return Encryption{}, ErrNotFound
}

func pathJoin(s ...string) string {
	return filepath.ToSlash(filepath.Join(s...))
}

func init() {
	emkit.Register(encryptionConfigKey, loadEncryptionKeys, nil, "Loads  pem encoded encryption keys from provided Add structs")
}
