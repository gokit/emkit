package databox_test

import (
	"testing"

	"github.com/gokit/emkit"
	"github.com/gokit/emkit/databox"
	"github.com/stretchr/testify/assert"
)

func TestGetBox(t *testing.T) {
	bucket := emkit.Bucket("myapp")
	defer bucket.Discarded()

	assert.NoError(t, databox.LoadFiles(bucket, databox.Add{
		Name:     "keys.priv",
		Dir:      "./data",
		FileName: "prv",
	}, databox.Add{
		Name:     "keys.pub",
		Dir:      "./data",
		FileName: "pub",
	}))

	box, err := databox.GetDataBox(bucket)
	assert.NoError(t, err)
	assert.NotNil(t, box)

	prv, err := box.Box("keys.priv")
	assert.NoError(t, err)
	assert.NotNil(t, prv)
	assert.NotEmpty(t, prv)

	pub, err := box.Box("keys.pub")
	assert.NoError(t, err)
	assert.NotNil(t, pub)
	assert.NotEmpty(t, pub)
}
