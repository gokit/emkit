Databox
---------
Load your data files into memory with configuration buckets with ease.


## Usage

```go
import (
	"github.com/gokit/emkit"
	"github.com/gokit/emkit/databox"
	"log"
)

bucket := emkit.Bucket("myapp")
defer bucket.Discarded()

err := databox.LoadFiles(bucket, databox.Add{
    Name:     "keys.date",
    Dir:      "./data",
    FileName: "prv",
}, databox.Add{
    Name:     "keys.pub",
    Dir:      "./data",
    FileName: "pub",
})

if err != nil {
    log.Fatal(err)
}

box, err := databox.GetDataBox(bucket)
if err != nil {
    log.Fatal(err)
}


// Get byte slice of data.
priv, err := box.Box("keys.date")
if err != nil {
    log.Fatal(err)
}
```