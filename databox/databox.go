package databox

import (
	"io/ioutil"
	"path/filepath"

	"fmt"

	"github.com/gokit/emkit"
)

// errors
var (
	ErrNoAddProvided      = Error("No Add instructions provided")
	ErrNotFound           = Error("Not Found")
	ErrNoNameProvided     = Error("Add.Name not provided")
	ErrNoFileNameProvided = Error("Add.FileName not provided")
)

// Error implements error interface.
type Error string

// Error returns a string version of error.
func (e Error) Error() string {
	return string(e)
}

//******************************************************
// Box and DataBox
//******************************************************

const (
	dataConfigKey = emkit.ConfigID("-config")
)

// DataBox defines an interface which exposes a method to
// retrieve a giving  key(public and private) loaded during
// setup to allow access.
type DataBox interface {
	Box(name string) ([]byte, error)
}

// Add embodies data used to load giving data file
// located within specified directory names. It provides
// associated fields to hold values of directory
// public key and private key name, which if either one is
// non-present then that key would not be loaded.
// it will be skipped as well.
type Add struct {
	Name     string
	Dir      string
	FileName string
}

func (c Add) valid() error {
	if c.Name == "" {
		return ErrNoNameProvided
	}
	if c.FileName == "" {
		return ErrNoFileNameProvided
	}
	return nil
}

// GetDataBox attempts to retrieve loaded DataBox from bucket
// if available else returning an error.
func GetDataBox(bucket *emkit.ConfigBucket) (DataBox, error) {
	item, err := bucket.Get(dataConfigKey)
	if err != nil {
		return nil, err
	}
	return item.(DataBox), nil
}

// LoadFiles will initializes giving bucket with a DataBox containing
// all loaded  data from provided Add slice. It returns an error if
// any failed and if successful attaches DataBox to bucket.
func LoadFiles(bucket *emkit.ConfigBucket, sets ...Add) error {
	if bucket.Discarded() {
		return emkit.ErrDiscardedBucket
	}
	if bucket.Has(dataConfigKey) {
		return nil
	}
	return bucket.Attach(dataConfigKey, sets)
}

func loadFiles(bucket *emkit.ConfigBucket, sets ...Add) (DataBox, error) {
	if len(sets) == 0 {
		return nil, ErrNoAddProvided
	}

	box := make(Box)

	for _, elem := range sets {
		if err := elem.valid(); err != nil {
			return nil, fmt.Errorf("add for %q is invalid: %+q", elem.Name, err)
		}

		data, err := ioutil.ReadFile(pathJoin(elem.Dir, elem.FileName))
		if err != nil {
			return nil, err
		}

		box[elem.Name] = data
	}

	return box, nil
}

type Box map[string][]byte

func (b Box) Box(name string) ([]byte, error) {
	if db, ok := b[name]; ok {
		return db, nil
	}
	return nil, ErrNotFound
}

func pathJoin(s ...string) string {
	return filepath.ToSlash(filepath.Join(s...))
}

func init() {
	emkit.Register(dataConfigKey, loadFiles, nil, "Loads data files from provided Add structs")
}
