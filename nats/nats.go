package nats

import (
	"errors"

	"github.com/gokit/emkit"
	gnats "github.com/nats-io/go-nats"
	natstream "github.com/nats-io/go-nats-streaming"
)

// configuration types.
const (
	streamingID    = emkit.ConfigID("NATS:Streaming")
	nonStreamingID = emkit.ConfigID("NATS:Conn")
)

//*****************************************************
// NATS Streaming
//*****************************************************

// InitStream initiates nats streamer into provided bucket using createStreamer
// as configuration.
func InitStream(bucket *emkit.ConfigBucket, args ...interface{}) error {
	if conn, err := bucket.Get(streamingID); err == nil && conn != nil {
		return nil
	}

	return bucket.Attach(streamingID, args...)
}

// GetStream retrieves the nats connection from the provided bucket,
// returning an error if not found.
func GetStream(bucket *emkit.ConfigBucket) (natstream.Conn, error) {
	if conn, err := bucket.Get(streamingID); err == nil && conn != nil {
		return conn.(natstream.Conn), nil
	}
	return nil, errors.New("not found")
}

// createStreamer handles the necessary logic to create a nat streaming connection.
func createStreamer(bucket *emkit.ConfigBucket, natClusterID string, natServerID string, ops ...natstream.Option) (natstream.Conn, error) {
	return natstream.Connect(natClusterID, natServerID, ops...)
}

// closeStream will close existing nats streaming connection in bucket.
func closeStream(bucket *emkit.ConfigBucket) {
	if conn, err := bucket.Get(streamingID); err == nil && conn != nil {
		conn.(natstream.Conn).Close()
	}
}

//*****************************************************
// NATS Connection
//*****************************************************

// InitNATS initiates nats streamer into provided bucket using createNATS
// as configuration.
func InitNATS(bucket *emkit.ConfigBucket, args ...interface{}) error {
	if conn, err := bucket.Get(nonStreamingID); err == nil && conn != nil {
		return nil
	}
	return bucket.Attach(nonStreamingID, args...)
}

// GetNATS retrieves the nats connection from the provided bucket,
// returning an error if not found.
func GetNATS(bucket *emkit.ConfigBucket) (*gnats.Conn, error) {
	if conn, err := bucket.Get(nonStreamingID); err == nil && conn != nil {
		return conn.(*gnats.Conn), nil
	}
	return nil, errors.New("not found")
}

// createNATS handles the necessary logic to create a normal nats connection.
func createNATS(bucket *emkit.ConfigBucket, url string, ops ...gnats.Option) (*gnats.Conn, error) {
	return gnats.Connect(url, ops...)
}

// closeNATS will close existing nats connection in bucket.
func closeNATS(bucket *emkit.ConfigBucket) {
	if conn, err := bucket.Get(nonStreamingID); err == nil && conn != nil {
		conn.(*gnats.Conn).Close()
	}
}

func init() {
	emkit.Register(nonStreamingID, createNATS, closeNATS)
	emkit.Register(streamingID, createStreamer, closeStream)
}
