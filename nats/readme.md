NATS
---------
Load your NATS/NATStreaming client connection into configuration buckets with ease.


## Usage

```go
import (
    "log"
    "github.com/gokit/emkit"
    "github.com/some/config/nats"
)

var (
    natsId = "waller"
    clusterId = "gosh"
)

func main(){
    // once a bucket is created with a unique name, it is
    // remembered and can be used again to get the same bucket
    // again.
    bucket := emkit.Bucket("servo-service")
    defer bucket.Discard();

    if err := nats.InitNATStreamer(bucket, clusterId, natsId); err != nil {
        log.Fatal(err)
    }

    conn := nats.GetNATStreamer(bucket)
    // do something with conn.
```