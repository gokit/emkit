package nats_test

import (
	"testing"

	"flag"

	"github.com/gokit/emkit"
	"github.com/gokit/emkit/nats"
	gonatstream "github.com/nats-io/go-nats-streaming"
	"github.com/stretchr/testify/assert"
)

var (
	natURL = flag.String("nats", "", "sets nats server URl to be used for test")
)

func TestCreateNAT(t *testing.T) {
	if *natURL == "" {
		t.Skip("Test require setting a -nats flag pointed to running nats server")
		return
	}

	mybucket := emkit.Bucket("my-config-1")

	assert.NoError(t, nats.InitNATS(mybucket, *natURL))

	conn, err := nats.GetNATS(mybucket)
	assert.NoError(t, err)
	assert.NotNil(t, conn)

	mybucket.Discard()

	assert.True(t, conn.IsClosed())
}

func TestCreateStreamer(t *testing.T) {
	if *natURL == "" {
		t.Skip("Test require setting a -nats flag pointed to running nats server")
		return
	}

	mybucket := emkit.Bucket("my-config-2")

	assert.NoError(t, nats.InitStream(mybucket, "nat-co", "nat-id", gonatstream.NatsURL(*natURL)))

	conn, err := nats.GetStream(mybucket)
	assert.NoError(t, err)
	assert.NotNil(t, conn)

	mybucket.Discard()

	assert.True(t, conn.NatsConn().IsClosed())
}
