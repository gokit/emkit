package emkit

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestList(t *testing.T) {
	var testdrafts = masterDraft{
		drafts: map[ConfigID]draft{},
	}

	id := ConfigID("TestTool")
	id2 := ConfigID("TestTool2")
	assert.NoError(t, testdrafts.Register(id, func(bucket *ConfigBucket) (string, error) {
		return "testers-tools", nil
	}, nil))
	assert.NoError(t, testdrafts.Register(id2, func(bucket *ConfigBucket) (string, error) {
		return "testers-tools", nil
	}, nil, "BOB"))
	assert.NotEmpty(t, testdrafts.List())
	assert.Equal(t, "NO DESCRIPTION", testdrafts.List()[string(id)])
	assert.Equal(t, "BOB", testdrafts.List()[string(id2)])
}

func TestRegister(t *testing.T) {
	var testdrafts = masterDraft{
		drafts: map[ConfigID]draft{},
	}

	id := ConfigID("TestTool")
	assert.Error(t, testdrafts.Register(id, func() {}, nil))
	assert.Error(t, testdrafts.Register(id, func() (string, string) {
		return "", ""
	}, nil))
	assert.NoError(t, testdrafts.Register(id, func(bucket *ConfigBucket) string {
		return "testers-tools"
	}, nil))
	assert.NoError(t, testdrafts.Register(id, func(bucket *ConfigBucket) (string, error) {
		return "testers-tools", nil
	}, nil))
	assert.NoError(t, testdrafts.Register(id, func(bucket *ConfigBucket) (struct{}, error) {
		return struct{}{}, nil
	}, nil))
}
