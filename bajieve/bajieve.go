// Package bajieve implements a configuration option for setting up a badger + bleve option
// based on passed into configuration parameters that dictate which badger collection and
// index type to create.
package bajieve

import (
	"fmt"
	"path/filepath"

	"github.com/blevesearch/bleve"
	"github.com/blevesearch/bleve/mapping"
	"github.com/dgraph-io/badger"
	"github.com/gokit/emkit"
)

// constants of errors.
const (
	ErrNotFound    = Error("Not Found")
	ErrEmptyCreate = Error("No Create items provided")
)

// Error implements error interface.
type Error string

// Error returns a string version of error.
func (e Error) Error() string {
	return string(e)
}

//******************************************************
// Create
//******************************************************

// Create defines instruction for creating a
// a giving item with name and associated index
// mapping. It is used both as instructions due
// to it's generic-ness to instruct creation of
// either a bleve index with associated mappings
// or a badger db with associated bleve index with
// mappings.
type Create struct {
	Name  string
	Index IndexMapping
}

//******************************************************
// IndexSet
//******************************************************

const (
	indexSetKey = emkit.ConfigID("indexedSet")
)

// IndexSet defines an interface which is exposes a single
// method to retrieve all associated index for a giving
// key name.
type IndexSet interface {
	Get(id string) (bleve.Index, error)
}

// CreateIndexMapping defines a function type that returns a
// new index mapping to be added to a bleve index.
type IndexMapping func() mapping.IndexMapping

// GetIndexedSet returns the attached IndexSet with the bucket if found
// else an error.
func GetIndexSet(bucket *emkit.ConfigBucket) (IndexSet, error) {
	db, err := bucket.Get(indexSetKey)
	if err != nil {
		return nil, err
	}

	return db.(IndexSet), nil
}

// CreateIndexSet creates a IndexSet with provided keys
// into a associated config bucket for easy retrieval and
// passing.
func CreateIndexSet(bucket *emkit.ConfigBucket, storeDir string, sets ...Create) error {
	if bucket.Discarded() {
		return emkit.ErrDiscardedBucket
	}
	if bucket.Has(indexSetKey) {
		return nil
	}
	return bucket.Attach(indexSetKey, storeDir, sets)
}

func createIndexSet(bucket *emkit.ConfigBucket, storeDir string, sets ...Create) (IndexSet, error) {
	if len(sets) == 0 {
		return nil, ErrEmptyCreate
	}

	storeDir = filepath.ToSlash(storeDir)

	set := bleveIndexSet{}
	for _, elem := range sets {
		indexPath := filepath.ToSlash(filepath.Join(storeDir, fmt.Sprintf("%s.bleve", elem.Name)))

		var mapp mapping.IndexMapping
		if elem.Index != nil {
			mapp = elem.Index()
		} else {
			mapp = mapping.NewIndexMapping()
		}

		indexer, err := bleve.New(indexPath, mapp)
		if err != nil {
			if indexer, err = bleve.Open(indexPath); err != nil {
				return nil, err
			}
		}

		set[elem.Name] = indexer
	}

	return set, nil
}

// disposeIndexSet will retrieved existing IndexSet from
// bucket, close all index and remove their references.
func disposeIndexSet(bucket *emkit.ConfigBucket) {
	if set, err := bucket.Get(indexSetKey); err == nil {
		iset := set.(bleveIndexSet)
		for key, elem := range iset {
			elem.Close()
			delete(iset, key)
		}
	}
}

type bleveIndexSet map[string]bleve.Index

func (b bleveIndexSet) Get(name string) (bleve.Index, error) {
	if db, ok := b[name]; ok {
		return db, nil
	}
	return nil, ErrNotFound
}

//******************************************************
// Badger + Bleve DB
//******************************************************

const (
	indexedBagerBleveKey = emkit.ConfigID("indexedBleve")
)

// BadgerIndexedDB defines an interface which is the
// value attached to a bucket which represents all badger
// and bleve index associated with that collection.
type BadgerIndexedDB interface {
	Get(db string) (IBadger, error)
}

// IBadger defines the configuration value type received
// from an associated bucket which hosts the configured
// badger and associated index. Also providing a method
// to close both underline badger db and bleve index if
// provided.
type IBadger interface {
	DB() *badger.DB
	Index() bleve.Index
	Close()
}

// GetBadgerIndexedDB returns the attached BadgerIndexDB with the bucket if found
// else an error.
func GetBadgerIndexedDB(bucket *emkit.ConfigBucket) (BadgerIndexedDB, error) {
	db, err := bucket.Get(indexedBagerBleveKey)
	if err != nil {
		return nil, err
	}

	return db.(BadgerIndexedDB), nil
}

// CreateBadger creates a configured Badger + Bleve setup which can be retrieved from
// provided bucket matching
// Where:
//
//	- storeDir: defines the directory path to where all badger db collections are stored.
//
//	- ...Install: defines the different collection instructions to be created.
func CreateBadger(bucket *emkit.ConfigBucket, storeDir string, op badger.Options, sets ...Create) error {
	if bucket.Discarded() {
		return emkit.ErrDiscardedBucket
	}
	if bucket.Has(indexedBagerBleveKey) {
		return nil
	}
	return bucket.Attach(indexedBagerBleveKey, storeDir, op, sets)
}

func createBadger(bucket *emkit.ConfigBucket, storeDir string, op badger.Options, inst ...Create) (BadgerIndexedDB, error) {
	if len(inst) == 0 {
		return nil, ErrEmptyCreate
	}

	storeDir = filepath.ToSlash(storeDir)

	dbs := indexBadgerMap{}

	for _, elem := range inst {
		dbOp := op

		dbPath := filepath.ToSlash(filepath.Join(storeDir, fmt.Sprintf("%s.badger", elem.Name)))
		indexPath := filepath.ToSlash(filepath.Join(storeDir, fmt.Sprintf("%s.bleve", elem.Name)))
		dbOp.Dir = dbPath
		dbOp.ValueDir = dbPath

		db, err := badger.Open(dbOp)
		if err != nil {
			return nil, err
		}

		var idb indexedBadger
		idb.db = db

		if elem.Index != nil {
			if mapping := elem.Index(); mapping != nil {
				indexer, err := bleve.New(indexPath, mapping)
				if err != nil {
					if indexer, err = bleve.Open(indexPath); err != nil {
						return nil, err
					}
				}

				idb.index = indexer
			}
		}

		dbs[elem.Name] = &idb
	}

	return dbs, nil
}

// disposeBadger will attempt to retrieve the BadgerIndexedDB instance within
// bucket and will ensure to close all associated badger and if present bleve
// indexes, deleting all references to objects as well.
func disposeBadger(bucket *emkit.ConfigBucket) {
	if set, err := bucket.Get(indexedBagerBleveKey); err == nil {
		iset := set.(indexBadgerMap)
		for key, elem := range iset {
			elem.Close()
			delete(iset, key)
		}
	}
}

type indexBadgerMap map[string]*indexedBadger

func (idm indexBadgerMap) Get(db string) (IBadger, error) {
	if db, ok := idm[db]; ok {
		return db, nil
	}
	return nil, ErrNotFound
}

type indexedBadger struct {
	db    *badger.DB
	index bleve.Index
}

func (id *indexedBadger) Close() {
	// defer here incase the index or db was already
	// closed which would cause panic.
	defer func() {
		recover()
	}()

	if id.db != nil {
		id.db.Close()
	}

	if id.index != nil {
		id.index.Close()
	}
}

func (id *indexedBadger) Index() bleve.Index {
	return id.index
}

func (id *indexedBadger) DB() *badger.DB {
	return id.db
}

func init() {
	emkit.Register(indexSetKey, createIndexSet, disposeIndexSet, "Creates a bleve index sets")
	emkit.Register(indexedBagerBleveKey, createBadger, disposeBadger, "Creates a badger db with an optional bleve index associated to name")
}
