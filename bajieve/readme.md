Bajieve
---------
Load your badger database and bleve indexes into configuration buckets with ease.


## Usage

```go
import (
	"github.com/gokit/emkit"
	"github.com/gokit/emkit/bajieve"
	"log"
)

bucket := emkit.Bucket("myapp-545")
if err := bajieve.CreateBadger(bucket, dir, badger.DefaultOptions, bajieve.Create{
    Name: "demo",
    Index: func() mapping.IndexMapping {
        return mapping.NewIndexMapping()
    },
}); err != nil {
    log.Fatal(err)
}

collection, err := bajieve.GetBadgerIndexedDB(bucket)
if err != nil {
    log.Fatal(err)
}

demo, err := collection.Get("demo")
demo.Index() // get the associated bleve index.
demo.DB() // get the associated badger db.
```