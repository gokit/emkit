package bajieve_test

import (
	"testing"

	"os"

	"io/ioutil"

	"github.com/blevesearch/bleve/mapping"
	"github.com/dgraph-io/badger"
	"github.com/gokit/emkit"
	"github.com/gokit/emkit/bajieve"
	"github.com/stretchr/testify/assert"
)

func TestIndexSet(t *testing.T) {
	dir, err := ioutil.TempDir("", "bajieve")
	assert.NoError(t, err)
	assert.NotEmpty(t, dir)

	defer os.RemoveAll(dir)

	bucket := emkit.Bucket("app-54434")
	err = bajieve.CreateIndexSet(bucket, dir, bajieve.Create{
		Name: "wataloo",
		Index: func() mapping.IndexMapping {
			return mapping.NewIndexMapping()
		},
	}, bajieve.Create{
		Name: "springlee",
		Index: func() mapping.IndexMapping {
			return mapping.NewIndexMapping()
		},
	})
	assert.NoError(t, err)

	set, err := bajieve.GetIndexSet(bucket)
	assert.NoError(t, err)
	assert.NotNil(t, set)

	dagger, err := set.Get("dagger")
	assert.Error(t, err)
	assert.Nil(t, dagger)

	wataloo, err := set.Get("wataloo")
	assert.NoError(t, err)
	assert.NotNil(t, wataloo)

	springlee, err := set.Get("springlee")
	assert.NoError(t, err)
	assert.NotNil(t, springlee)

	bucket.Discard()
	_, err = bajieve.GetIndexSet(bucket)
	assert.Error(t, err)
}

func TestIndexedBadgerSet(t *testing.T) {
	dir, err := ioutil.TempDir("", "bajieve")
	assert.NoError(t, err)
	assert.NotEmpty(t, dir)

	defer os.RemoveAll(dir)

	bucket := emkit.Bucket("app-2323")
	err = bajieve.CreateBadger(bucket, dir, badger.DefaultOptions, bajieve.Create{
		Name: "wataloo",
		Index: func() mapping.IndexMapping {
			return mapping.NewIndexMapping()
		},
	}, bajieve.Create{
		Name: "springlee",
		Index: func() mapping.IndexMapping {
			return mapping.NewIndexMapping()
		},
	})
	assert.NoError(t, err)

	set, err := bajieve.GetBadgerIndexedDB(bucket)
	assert.NoError(t, err)
	assert.NotNil(t, set)

	dagger, err := set.Get("dagger")
	assert.Error(t, err)
	assert.Nil(t, dagger)

	wataloo, err := set.Get("wataloo")
	assert.NoError(t, err)
	assert.NotNil(t, wataloo)
	assert.NotNil(t, wataloo.Index())
	assert.NotNil(t, wataloo.DB())

	springlee, err := set.Get("wataloo")
	assert.NoError(t, err)
	assert.NotNil(t, springlee)
	assert.NotNil(t, springlee.Index())
	assert.NotNil(t, springlee.DB())

	bucket.Discard()
	_, err = bajieve.GetIndexSet(bucket)
	assert.Error(t, err)
}

func TestSafeClosingIndexedBadgerSet(t *testing.T) {
	dir, err := ioutil.TempDir("", "bajieve")
	assert.NoError(t, err)
	assert.NotEmpty(t, dir)

	defer os.RemoveAll(dir)

	bucket := emkit.Bucket("app-3")
	defer bucket.Discard()

	err = bajieve.CreateBadger(bucket, dir, badger.DefaultOptions, bajieve.Create{
		Name: "damma",
		Index: func() mapping.IndexMapping {
			return mapping.NewIndexMapping()
		},
	})
	assert.NoError(t, err)

	set, err := bajieve.GetBadgerIndexedDB(bucket)
	assert.NoError(t, err)
	assert.NotNil(t, set)

	wataloo, err := set.Get("damma")
	assert.NoError(t, err)
	assert.NotNil(t, wataloo)
	assert.NotNil(t, wataloo.Index())
	assert.NotNil(t, wataloo.DB())

	_, err = wataloo.Index().DocCount()
	assert.NoError(t, err)

	wataloo.Close()

	_, err = wataloo.Index().DocCount()
	assert.Error(t, err)
}

func TestSafeClosingIndexedBadgerSetWithDiscard(t *testing.T) {
	dir, err := ioutil.TempDir("", "bajieve")
	assert.NoError(t, err)
	assert.NotEmpty(t, dir)

	defer os.RemoveAll(dir)

	bucket := emkit.Bucket("myapp-545")
	err = bajieve.CreateBadger(bucket, dir, badger.DefaultOptions, bajieve.Create{
		Name: "demo",
		Index: func() mapping.IndexMapping {
			return mapping.NewIndexMapping()
		},
	})
	assert.NoError(t, err)

	set, err := bajieve.GetBadgerIndexedDB(bucket)
	assert.NoError(t, err)
	assert.NotNil(t, set)

	wataloo, err := set.Get("demo")
	assert.NoError(t, err)
	assert.NotNil(t, wataloo)
	assert.NotNil(t, wataloo.Index())
	assert.NotNil(t, wataloo.DB())

	_, err = wataloo.Index().DocCount()
	assert.NoError(t, err)

	bucket.Discard()

	_, err = wataloo.Index().DocCount()
	assert.Error(t, err)
}
